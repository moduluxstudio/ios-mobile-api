//
//  API.swift
//  module-mobile-api
//
//  Created by Jonathan  Silva on 08/09/17.
//  Copyright © 2017 Modulux Studio. All rights reserved.
//

import Foundation

public enum Stage : String {
    case develop
    case testing
    case staging
    case production
}

public struct ApiStage {
    public let stage : Stage
    public let url : String
    public let version : String
    
    public init(stage: Stage, url: String, version: String) {
        self.stage = stage
        self.url = url
        self.version = version
    }
}

public class API {
    
    let api : ApiStage
    
    public init(apiStaging : ApiStage) {
        api = apiStaging
    }
    
    public func route(_ endPoint: String) -> String {
        let url = String(format: "%@/%@/%@",api.url, api.version, endPoint)
        return url
    }
    
}

