#
#  Be sure to run `pod spec lint moduleMobileApi.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see http://docs.cocoapods.org/specification.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

Pod::Spec.new do |s|

  s.name         = "ModuleMobileApi"
  s.version      = "0.0.1"
  s.summary      = "A three-ring control like the Activity status bars"
  s.description  = "The three-ring is a completely customizable widget that can be used in any iOS app. It also plays a little victory fanfare."
  s.homepage     = "http://raywenderlich.com"
  s.license      = "MIT"
  s.platform     = :ios, "9.0"
  s.authors      = { "Jonathan Silva Figueroa" => "" }
  s.source       = { :path => '.' }
  s.source_files = "ModuleMobileApi", "ModuleMobileApi/**/*.{h,m,swift}"
  s.pod_target_xcconfig = { 'SWIFT_VERSION' => '3' }
end
