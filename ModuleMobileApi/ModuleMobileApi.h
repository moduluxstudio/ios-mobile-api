//
//  ModuleMobileApi.h
//  ModuleMobileApi
//
//  Created by Jonathan  Silva on 08/09/17.
//  Copyright © 2017 Modulux Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for ModuleMobileApi.
FOUNDATION_EXPORT double ModuleMobileApiVersionNumber;

//! Project version string for ModuleMobileApi.
FOUNDATION_EXPORT const unsigned char ModuleMobileApiVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <ModuleMobileApi/PublicHeader.h>


